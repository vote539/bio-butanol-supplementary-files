function [ J ] = pdms_flux(P, T, acet, buoh, etoh)
% Compute the flux of acetone, butanol, ethanol, and water across a
% silicalite-1/PDMS hybrid 100-micron pervaporation membrane.
%
% Parameters:
%   P => vacuum pressure (range: 310 Pa to 4000 Pa)
%   T => temperature (range: 306 K to 342 K)
%   acet => acetone concentration (range: 0.0 to 2.5 wt%)
%   buoh => butanol concentration (range: 0.0 to ? wt%)
%   etoh => ethanol concentration (range: 0.1 to 2.3 wt%)
%
% Capable of accepting a one-column-many-row vector as *one* of the arguments.
%
% Return value: a vector containing the fluxes of each species (g/m2/hr):
%   J = [J_acet, J_buoh, J_etoh, J_water]
%
% CITATION:
% Haoli Zhou, Yi Su, Xiangrong Chen, Yinhua Wan, Separation of acetone, butanol and ethanol (ABE) from dilute aqueous solutions by silicalite-1/PDMS hybrid pervaporation membranes, Separation and Purification Technology, Volume 79, Issue 3, 24 June 2011, Pages 375-384, ISSN 1383-5866, http://dx.doi.org/10.1016/j.seppur.2011.03.026.
% (http://www.sciencedirect.com/science/article/pii/S1383586611001766)
% Keywords: Pervaporation; Acetone–butanol–ethanol (ABE); Silicalite-1; Polydimethylsiloxane (PDMS); Hansen solubility parameter

% Load in the data
source('pdms_data.m');

% Turn off broadcasting warnings
warning('off', 'Octave:broadcast');

% Shortcut interpolation functions
inter = @(mat,x) interp1(mat(:,1), mat(:,2), x, 'linear');
scale = @(mat,x,x0) inter(mat,x) / inter(mat,x0);

% Scale on temperature
J_acet  = J_acet  .* scale( acet_T,T,T0); % g/m2/hr
J_buoh  = J_buoh  .* scale( buoh_T,T,T0); % g/m2/hr
J_etoh  = J_etoh  .* scale( etoh_T,T,T0); % g/m2/hr
J_water = J_water .* scale(water_T,T,T0); % g/m2/hr

% Scale on acetone fraction
J_acet  = J_acet  .* scale( acet_acet,acet,acet0); % g/m2/hr
J_buoh  = J_buoh  .* scale( buoh_acet,acet,acet0); % g/m2/hr
J_etoh  = J_etoh  .* scale( etoh_acet,acet,acet0); % g/m2/hr
J_water = J_water .* scale(water_acet,acet,acet0); % g/m2/hr

% Scale on ethanol fraction
J_acet  = J_acet  .* scale( acet_etoh,etoh,etoh0); % g/m2/hr
J_buoh  = J_buoh  .* scale( buoh_etoh,etoh,etoh0); % g/m2/hr
J_etoh  = J_etoh  .* scale( etoh_etoh,etoh,etoh0); % g/m2/hr
J_water = J_water .* scale(water_etoh,etoh,etoh0); % g/m2/hr

% Scale on pressure
a = [inter(etoh_P,P), inter(acet_P,P), inter(buoh_P,P)];
J0 = inter(J0_P,P);
Ji = Jai(a,J0);
Jw = Jaw(a,J0);
a0 = [inter(etoh_P,P0), inter(acet_P,P0), inter(buoh_P,P0)];
J00 = inter(J0_P,P0);
Ji0 = Jai(a0,J00);
Jw0 = Jaw(a0,J00);

J_acet  = J_acet  .* Ji(:,2)/Ji0(2); % g/m2/hr
J_buoh  = J_buoh  .* Ji(:,3)/Ji0(3); % g/m2/hr
J_etoh  = J_etoh  .* Ji(:,1)/Ji0(1); % g/m2/hr
J_water = J_water .* Jw/Jw0; % g/m2/hr

% Scale on butanol
% No data given in the paper, so assume perfect linear relationship.
J_buoh  = J_buoh  .* buoh./buoh0;
J_acet  = J_acet  .* ones(size(buoh));
J_etoh  = J_etoh  .* ones(size(buoh));
J_water = J_water .* ones(size(buoh));

% Return data
J = [J_acet, J_buoh, J_etoh, J_water];

end































