Team STCK Supplementary Files
=============================

The file "HYSYS-Case.hsc" contains the full HYSYS case used to simulate the product separation process.

The three *.m files contain the data and code used to simulate flux across the pervaporation membrane.  The file "pdms_pipe.m" is a script that you can run normally.  The file "pdms_flux.m" is a function that uses experimental data from "pdms_data.m" to estimate the mass flux of species across the membrane.

To download the files, click the "Download" button in the left menu.  It looks like a cloud with an arrow pointing down.  If you are familiar with Git, you can use the clone URL near the top right corner of this page.