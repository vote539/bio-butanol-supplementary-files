% Calculate the permeate and retentate compositions through a membrane pipe.
%
% SPECIES ORDER: acetone, butanol, ethanol, water
%
% SYMBOLOGY:
%    m => mass flow rate (g/hr)
%    C => concentration (g/m3)
%    dA => perimeter of pipe (m)
%    Q => volumetric flow rate (m3/hr)
%    P => permeate pressure (Pa)
%    T => operation temperature (K)

%%%%% ENTER DATA HERE %%%%%

% INLET MASS FLOWS
m_in = [
13359.8275
41360.7755
 4462.4384
43862.9042
] * 454;
m_in = m_in'; #'

% Pipe Perimeter
diam = 3e-3; % m
npipes = 1e6;
dA = diam*pi*npipes; % m

% Max Length of Pipe
L = 2; % m

% Desired Recovery of Butanol
recov = 0.95;

%%%%% BEGIN COMPUTATION %%%%%

% Function to Convert Mass Flow Rates -> Concentrations
rho = [0.791, 0.810, 0.789, 1.000] * 1e6; % g/m3
m_to_C = @(m) m ./ sum(m ./ rho);

% Function to Convert Concentrations -> Mass Percentages
C_to_w = @(C) C ./ sum(C) .* 100;

% Volumetric Flow Rate
Q = sum(m_in ./ rho);

% Differential Equation
P = 310; % Pa
T = 342; % K
dCdx = @(C,x) -dA/Q*pdms_flux(P,T,C_to_w(C)(1),C_to_w(C)(2),C_to_w(C)(3));

% Solve the system
xx = linspace(0,L,100);
soln = lsode(@(C,x) dCdx(C,x), m_to_C(m_in), xx);

% Find the specified recovery point
Cin = soln(1,:);
Cstar = Cin(2)*(1-recov);
Lstar = interp1(soln(:,2), xx, Cstar)
Astar = Lstar*dA

% Display results
Cout = soln(length(xx),:);
disp('Flow Rates in lb/hr (acetone, butanol, ethanol, water):');
inlet_flow = m_in/454 % lb/hr
retentate_flow = Cout*Q/454 % lb/hr
permeate_flow = (m_in-Cout*Q)/454 % lb/hr
permeate_mass_frac = permeate_flow ./ sum(permeate_flow)
permeate_flow_frac = sum(permeate_flow)/sum(inlet_flow)
permeate_buoh_recovery = permeate_flow(2)/inlet_flow(2)
fprintf('Membrane Area: %6.4f m^2\n', dA*L);


