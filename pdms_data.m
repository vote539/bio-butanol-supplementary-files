% Baseline Parameters (based on Figure 11)
T0 = 323; % K
P0 = 310; % Pa
acet0 = 0.5; % wt%
buoh0 = 1.0; % wt%
etoh0 = 0.5; % wt%
J_acet = 8.18; % g/m2/hr
J_buoh = 22.6; % g/m2/hr
J_etoh = 1.64; % g/m2/hr
J_water = 29.6; % g/m2/hr

% Baseline flow rate: 2.0 L/min

% Figure 5a(i)
% 3.5 wt% acetone
acet_T = [
    306 50
    314 68
    323 90
    333 120
    342 160
];

% Figure 5b(i)
% 2.6 wt% butanol
buoh_T = [
    306 14
    315 28
    323 41
    333 61
    343 96
];

% Figure 5b(ii)
water_T = [
    306 18
    315 25
    323 32
    333 50
    343 70
];

% Figure 5c(i)
% 4.0 wt% ethanol
etoh_T = [
    306 10
    314 15
    322 22
    332 28
    343 48
];

% Figure 10a(i)
water_acet = [
    0.0 32
    0.3 31
    0.7 30
    1.1 29
    1.5 28
    2.0 27
    2.5 27
    30  27 % extrapolated
];

% Figure 10a(i)
buoh_acet = [
    0.0 26
    0.3 26
    0.7 28
    1.1 29
    1.5 30
    2.0 31
    2.5 32
    30  32 % extrapolated
];

% Figure 10a(i)
acet_acet = [
    0.0 0
    0.3 7
    0.7 13
    1.1 19
    1.5 27
    2.0 36
    2.5 44
    30  528 % extrapolated
];

% Figure 10a(ii)
etoh_acet = [
    0.0 1.10
    0.3 1.10
    0.7 1.13
    1.1 1.18
    1.5 1.21
    2.0 1.23
    2.5 1.27
    30  1.27 % extrapolated
];

% Figure 11a(i)
water_etoh = [
    0.0 31.1 % extrapolated
    0.1 31.1
    0.5 29.6
    0.9 29.6
    1.4 27.4
    1.8 26.7
    2.3 25.2
    30  25.2 % extrapolated
];

% Figure 11a(i)
buoh_etoh = [
    0.0 22.2 % extrapolated
    0.1 22.2
    0.5 22.6
    0.9 23.7
    1.4 24.1
    1.8 25.2
    2.3 25.9
    30  25.9 % extrapolated
];

% Figure 11a(ii)
etoh_etoh = [
    0.0 0.00 % extrapolated
    0.1 0.54
    0.5 1.64
    0.9 3.27
    1.4 4.82
    1.8 6.18
    2.3 8.09
    30  105.52 % extrapolated
];

% Figure 11a(ii)
acet_etoh = [
    0.1 8.00 % extrapolated
    0.1 8.00
    0.5 8.18
    0.9 8.68
    1.4 8.82
    1.8 9.00
    2.3 9.14
    30  9.14 % extrapolated
];

% Figure 12 is given in terms of separation factor ai.
% The relationship between flux J and ai is given by the following expressions.
wif = [.15 .5 1]; % ethanol, acetone, butanol wt% in feed
wwf = 100-sum(wif); % water wt% in feed
% mass flux of species i (1=etoh, 2=acet, 3=buoh) given vector of a values
Jai = @(a,J0) J0.*a.*wif./wwf.*(1-sum(a,2)./(wwf./wif+sum(a,2)));
% mass flux of water given vector of a values
Jaw = @(a,J0) J0 - sum(Jai(a,J0),2);

% Figure 12a
J0_P = [
    310  47.2
    600  37.8
    1000 28.3
    2000 16.7
    4000 11.7
];

% Figure 12b
acet_P = [
    310  54.6
    600  43.5
    1000 37.9
    2000 42.6
    4000 47.2
];

% Figure 12b
etoh_P = [
    310  14.8
    600  13.9
    1000 14.8
    2000 14.8
    4000 17.6
];

% Figure 12b
buoh_P = [
    310  75.0
    600  50.9
    1000 36.1
    2000 34.3
    4000 31.5
];
